# unsecure-webapp
 An unsecure web to showcase cybersecurity issues

## Installing the web app
* The web app is written using node and tested using version v19.7.0
* NPM is used as the package manager and testing using version v9.5.0

Installing the dependencies can be done via the following command
```
npm install
```

Running the app can be done via
```
npm run start
```
or 
```
npm run dev
```
 
## Bruteforce of UserAccounts
A script is added in the root directory `bruteforce.js`
It reads a file `rockyou-20.txt`(also in the root directory) which contains common passwords.

Two accounts are added in memory to showcase how easily common passwords can be retrieved through a simple script.
* username: devteam , password: mahal, role: admin
* username: niels, password: linkinpark, role: customer

For each password in the file, the html form from the login page (index.html) is called using the password as the credential. 
When a match is found, the script shows the matched password in the terminal.

to run the script simply submit the following command:
```
node bruteforce.js
```



# Docker
## Building An Image

`docker build  . -t nille85/unsecure-webapp:0.0.5`

For GCP cloud run, specify platform ```--platform linux/amd64```

## Running A Container
Bind the port of the container to the port of your machine

`docker run -e PORT=3000 -p 3001:3000 nille85/unsecure-webapp:0.0.5`

# Prometheus
Run the following command from the web applications root directory
`docker run -it -p 9090:9090 -v $(pwd)/prometheus-data/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus`

# Grafana
`docker run -i -p 3000:3000 grafana/grafana`

Configure Prometheus as a datasource. Set the url to `http://host.docker.internal:9090` when Prometheus runs as a Docker container on your localhost.

Dashboards from the `grafana` folder can be imported into grafana. Make sure the datasource type and id matches.



# More Information
[Example project with Prometheus & Node](https://github.com/RisingStack/example-prometheus-nodejs)

An example dashboard:
[NodeJS Grafana Dashboard](https://grafana.com/grafana/dashboards/11956-nodejs-metrics/)

[PromQL Cheat Sheet](https://promlabs.com/promql-cheat-sheet/)


# Code Signing
[Signing commits](https://docs.gitlab.com/ee/user/project/repository/signed_commits/ssh.html)
[How and why to sign commits](https://withblue.ink/2020/05/17/how-and-why-to-sign-git-commits.html)


view Git configuration: 

```git config --list```

It's possible to impersonate someone else by configuring the `user.name` and `user.email` field to another individual. Signed commits solve this problem by proving that you are who you say you are. 

Configure Git to use SSH for commit signing:
```git config --global gpg.format ssh```

Specify which public SSH key to use as the signing key and change the filename (~/.ssh/id_rsa.pub) to the location of your key. The filename might differ, depending on how you generated your key:
```git config --global user.signingkey ~/.ssh/id_rsa.pub```

To sign a commit:

1. Use the -S flag when signing your commits:
```git commit -S -m "My commit msg"```

2. Optional. If you don’t want to type the -S flag every time you commit, tell Git to sign your commits automatically:
```git config --global commit.gpgsign true```




# Todos
* Visualize test reports in GitLab: needs ultimate edition
* Look into code signing