interface UserAccountRepository{
    save(userAccount: UserAccount) : void
    findByUsernameAndPassword(username: string, password: string) : UserAccount
}


interface UserAccountSessionRepository{
    createSession(username: string, password: string): UserAccountSession
    findByToken(token: string) : UserAccountSession
    isValidSession(token: string): boolean
    endSession(token: string) : void
}

type UserAccountSession = {
    token: string,
    username: string,
    role: Role,
    expiresAt: Date
}

type UserAccount = {
    username : string,
    password : string,
    role : Role
}

class Role{
    private name: string
    private permissions: Permission[]

    constructor(name: string, permissions: Permission[]){
        this.name = name
        this.permissions = permissions
    }
}

enum Permission {
    ViewCustomers = "view-customers",
    EditCustomers = "edit-customers",
    DeleteCustomers = "delete-customers",
    ViewMachines = "view-machines",
    EditMachines = "edit-machines",
    DeleteMachines = "delete-machines",
}


class UserAccountAlreadyExists extends Error{
    constructor(message: string) {
        super(message);
        this.name = "UserAccountAlreadyExists";
    }
}


export {Role, Permission, UserAccount, UserAccountRepository, UserAccountSession, UserAccountSessionRepository, UserAccountAlreadyExists}