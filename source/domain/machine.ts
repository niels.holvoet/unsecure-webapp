import { Customer } from "./customer"

type Machine = {
    id : string,
    data : MachineData
    
}

type MachineData = {
    type : MachineType
    customer : string,
    location: MachineLocation
}

enum MachineType{
   Textile = "Textile",
   Plastic = "Plastic"
}

type MachineLocation = {
    site : string 
}

interface MachineRepository{
    create(MachineData: MachineData) : Machine
    listAll() : Machine[]
}



export {Machine, MachineData, MachineType, MachineLocation, MachineRepository}