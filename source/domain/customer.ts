type Customer = {
    id : string,
    data : CustomerData
}

type CustomerData = {
    name: string
}

interface CustomerRepository{
    create(customerData: CustomerData) : Customer
    listAll() : Customer[]
    findByName(name: string) : Customer
}

export {Customer, CustomerData, CustomerRepository}