const dotenv = require('dotenv')

import express from 'express'
const cookieParser = require('cookie-parser')
import { instrumentedRoutes, instrumentRequest, instrumentResponse, instrumentError } from './routes/instrumentation'
import { publicRoutes } from './routes/public.routes'
import { privateRoutes } from './routes/private.routes'
import { TemplateRenderer } from './infrastructure/web/template/templateRenderer'
const htmlTemplateRenderer = TemplateRenderer.create()


dotenv.config()

const port = parseInt(process.env.PORT) || 8080
const app = express()

app.use(instrumentRequest)
app.use(instrumentedRoutes)
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(publicRoutes)
app.use("/private", privateRoutes)

app.use(instrumentResponse)

const errorHandler = (err: Error, req: any, res: any, next: any) => {
    instrumentError(err, req, res)
    console.error(err.stack)
    const content = htmlTemplateRenderer.render("templates/error.html")
    res.status(200).send(content)
    next()
}

app.use(errorHandler)

app.listen(port, () => console.log(`Running on port ${port}`))