
import { UserAccount, Role, Permission, UserAccountRepository, UserAccountSessionRepository } from './../domain/userAccount'
import { InMemoryUserAccountRepository } from "./../infrastructure/database/inMemoryUserAccountRepository"
import { InMemoryUserAccountSessionRepository } from "./../infrastructure/database/inMemoryUserAccountSessionRepository"


const adminRole : Role = new Role("Admin", [
  Permission.ViewCustomers,Permission.DeleteCustomers, Permission.EditCustomers,
  Permission.ViewMachines, Permission.DeleteMachines, Permission.EditMachines
])

const customerRole : Role = new Role("Customer", [
  Permission.ViewMachines, Permission.DeleteMachines, Permission.EditMachines
])

const userAccounts: UserAccount[] = [
  { username: "devteam", password: "mahal", role: adminRole },
  { username: "niels", password: "linkinpark", role: customerRole }
]
const userAccountRepository: UserAccountRepository = InMemoryUserAccountRepository.create(userAccounts)
const userAccountSessionRepository: UserAccountSessionRepository = InMemoryUserAccountSessionRepository.createEmpty(userAccountRepository)


export {userAccountRepository, userAccountSessionRepository}