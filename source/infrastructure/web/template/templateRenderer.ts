import Mustache from 'mustache'
import { FileReader } from '../../file/fileReader'


class TemplateRenderer {

    private fileReader: FileReader
    constructor(fileReader: FileReader) {
        this.fileReader = fileReader
    }

    static create(): TemplateRenderer {
        return new TemplateRenderer(FileReader.create())
    }


    render(templateLocation: string, data?: Object) {
        const template = this.fileReader.readFromLocation(templateLocation)
        const mustacheData = data || {}
        return Mustache.render(template, mustacheData)

    }


}

export { TemplateRenderer }