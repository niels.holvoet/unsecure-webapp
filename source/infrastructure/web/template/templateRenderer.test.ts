import { describe, expect, test, beforeEach, jest } from '@jest/globals'

import { TemplateRenderer } from './templateRenderer'
import { FileReader, FileNotFound } from './../../file/fileReader'



describe('render template', () => {

    let fileReader: FileReader
    beforeEach(() => {
        fileReader =
        {
            readFromLocation: jest.fn(() => ""),
            getAppRootDir: jest.fn(() => "")
        }
    })

    test('when template exists ', () => {
        const mock = jest.fn(() => {
            return "This is a test for username {{username}}"
        })
        fileReader.readFromLocation = mock


        const templateRenderer: TemplateRenderer = new TemplateRenderer(fileReader)
        const result = templateRenderer.render("/template/found.html", { username: "niels" })
        expect(mock).toHaveBeenCalledTimes(1);
        expect(result).toEqual("This is a test for username niels")

    })

    test('when fileReader throws an exception', () => {

        const mock = jest.fn((fileLocation) => {
            throw new FileNotFound(`file at ${fileLocation} could not be read, detail: more details`)
        })
        fileReader.readFromLocation = mock


        const templateRenderer: TemplateRenderer = new TemplateRenderer(fileReader)
        expect(mock).toThrowError
        expect(() => templateRenderer.render("/template/found.html", { username: "niels" })).toThrowError(new FileNotFound("file at /template/found.html could not be read, detail: more details"))

    })
})

