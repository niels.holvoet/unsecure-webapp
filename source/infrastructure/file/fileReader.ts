import fs from 'fs'
import path from 'path'

export class FileReader{


    static create(){
        return new FileReader()
    }

    readFromLocation(fileLocation : string) : string{
        const rootDir = this.getAppRootDir()
        try {
            const filePath = `${rootDir}/${fileLocation}`
            const template = fs.readFileSync(filePath, 'utf-8')
            return template
          } catch (err) {
            throw new FileNotFound(`file at ${fileLocation} could not be read, detail: ${err.message}`)
        }
    }

    getAppRootDir() : string {
        let currentDir = __dirname
        while(!fs.existsSync(path.join(currentDir, 'package.json'))) {
          currentDir = path.join(currentDir, '..')
        }
        return currentDir
    }
}

export class FileNotFound extends Error{
    constructor(message: string) {
        super(message);
        this.name = "FileNotFound";
    }
}
