import { describe, expect, test, beforeEach } from '@jest/globals'
import { FileReader, FileNotFound } from './fileReader'

describe('read file', () => {

    test('when file is found', () => {

        const fileReader: FileReader = FileReader.create()
        const fileLocation = "templates/tests/test.html"
        const contents = fileReader.readFromLocation(fileLocation)
        expect(contents).toEqual("This is a test")
    })

    test('when file is not found', () => {

        const fileReader: FileReader = FileReader.create()
        const fileLocation = "templates/tests/unexisting.html"
        expect(() => fileReader.readFromLocation(fileLocation)).toThrow(FileNotFound)
    })
})

describe('get application root dir', () => {
    test("rootDir is found", () => {
        const fileReader: FileReader = FileReader.create()
        expect(fileReader.getAppRootDir()).not.toBeUndefined()
    })

})
