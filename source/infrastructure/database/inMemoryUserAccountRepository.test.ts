import {describe, expect, test, beforeEach} from '@jest/globals'
import {InMemoryUserAccountRepository} from './inMemoryUserAccountRepository'
import {Role, UserAccount, UserAccountRepository, UserAccountAlreadyExists} from '../../domain/userAccount'

describe('save userAccount', () => {
  
    let adminRole: Role
    beforeEach(() => {
        adminRole = new Role("admin", [] )
    })


    test('does not throw error', () => {
        let repository : UserAccountRepository
        repository = InMemoryUserAccountRepository.createEmpty()
        const userAccount : UserAccount = {username : "john", password: "secret", role: adminRole}
        expect(() => repository.save(userAccount)).not.toThrowError()
    })

    test("when username and password already exists", () => {
        let repository : UserAccountRepository
        const userAccounts : UserAccount[] = [
            {username: "devteam", password: "mahal", role: adminRole}
        ]
        repository = InMemoryUserAccountRepository.create(userAccounts)
        const newUserAccount : UserAccount = {username: "devteam", password: "mahal", role: adminRole}
        expect(() => repository.save(newUserAccount)).toThrow(UserAccountAlreadyExists)
    
    })
})


describe ('findByUsernameAndPassword', () => {

    let adminRole: Role
    beforeEach(() => {
        adminRole = new Role("admin", [] )
    })

    test('when userAccount is found', () => {
        let repository : UserAccountRepository
        const userAccounts : UserAccount[] = [
            {username: "devteam", password: "mahal", role: adminRole},
            {username: "niels", password:"linkinpark", role: adminRole}
        ]
        repository = InMemoryUserAccountRepository.create(userAccounts)
        const foundUserAccount = repository.findByUsernameAndPassword("niels", "linkinpark")
        expect(foundUserAccount.username).toEqual("niels")
        expect(foundUserAccount.password).toEqual("linkinpark")

    })

    test('when userAccount is not found', () => {
        let repository : UserAccountRepository
        const userAccounts : UserAccount[] = [
            {username: "devteam", password: "mahal", role: adminRole},
            {username: "niels", password:"linkinpark", role: adminRole}
        ]
        repository = InMemoryUserAccountRepository.create(userAccounts)
        expect(repository.findByUsernameAndPassword("otheruser", "linkinpark")).toBeUndefined()

    })

})