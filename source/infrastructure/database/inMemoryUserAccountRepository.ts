import { UserAccount, UserAccountRepository, UserAccountAlreadyExists } from "../../domain/userAccount"

class InMemoryUserAccountRepository{

    private userAccounts: UserAccount[]

    constructor(userAccounts : UserAccount[]){
        this.userAccounts = userAccounts
    }

    static createEmpty() : UserAccountRepository{
        return new InMemoryUserAccountRepository([])
    }

    static create(userAccounts : UserAccount[]): UserAccountRepository{
        return new InMemoryUserAccountRepository(userAccounts)
    }


    save = (userAccount: UserAccount): void => {
        const{username, password} = userAccount
        const existingAccount = this.findByUsernameAndPassword(username, password)
        if(!existingAccount){
            this.userAccounts.push(userAccount)
        }else{
            throw new UserAccountAlreadyExists("An account already exists with the given username and passworcd")
        }
    }

    findByUsernameAndPassword = (username: string, password: string) : UserAccount => {
        const filteredAccounts = this.userAccounts.filter(userAccount => {
            return (userAccount.username == username && userAccount.password == password)
        })
        if(filteredAccounts.length !== 0){
            return filteredAccounts[0]
        }else{
           return undefined
        }

    }
}





export {InMemoryUserAccountRepository}