import { UserAccount, UserAccountRepository, UserAccountSession, UserAccountSessionRepository } from "../../domain/userAccount"
const uuid = require('uuid')

class InMemoryUserAccountSessionRepository implements UserAccountSessionRepository {

    private sessions: Map<string, UserAccountSession>
    private userAccountRepository: UserAccountRepository

    constructor(sessions: Map<string, UserAccountSession>, userAccountRepository: UserAccountRepository) {
        this.sessions = sessions
        this.userAccountRepository = userAccountRepository
    }


    static createEmpty(userAccountRepository: UserAccountRepository): UserAccountSessionRepository {
        return new InMemoryUserAccountSessionRepository(new Map<string, UserAccountSession>(), userAccountRepository)
    }
    createSession(username: string, password: string): UserAccountSession {
        const userAccount = this.userAccountRepository.findByUsernameAndPassword(username, password)
        if (userAccount) {
            const token = uuid.v4()
            const now = new Date()
            const expiresAt = new Date(+now + 120 * 1000)
            const userAccountSession: UserAccountSession = {
                token: token,
                username: username,
                role: userAccount.role,
                expiresAt: expiresAt
            }
            this.sessions.set(token, userAccountSession)
            return userAccountSession
        }
        return undefined

    }

    findByToken(token: string): UserAccountSession {
        const userAccountSession = this.sessions.get(token)
        return userAccountSession
    }

    isValidSession(token: string): boolean {
        if (token) {
            const userAccountSession = this.findByToken(token)
            if (userAccountSession) {
                const isNotExpired = userAccountSession.expiresAt > new Date()
                return isNotExpired
            }
        }
        else {
            return false
        }
    }

    endSession(token: string): void {
        this.sessions.delete(token)
    }



}

export { InMemoryUserAccountSessionRepository }