const uuid = require('uuid')
import {Machine, MachineData, MachineRepository} from "../../domain/machine"
class InMemoryMachineRepository implements MachineRepository{
    private machines: Machine[]
    
    constructor(machines: Machine[]){
        this.machines = machines
    }

    create(machineData: MachineData): Machine {
        const id: string = uuid.v4()
        const machine = {id, data: machineData}
        this.machines.push(machine)
        return machine;
    }
    listAll(): Machine[] {
        return this.machines
    }

}

export {InMemoryMachineRepository}