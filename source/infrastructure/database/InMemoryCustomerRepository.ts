
import { Customer, CustomerRepository, CustomerData  } from "../../domain/customer"
const uuid = require('uuid')
class InMemoryCustomerRepository implements CustomerRepository{
    private customers: Customer[]
    
    constructor(customers: Customer[]){
        this.customers = customers
    }
    findByName(name: string): Customer {
        return this.customers.filter(customer => customer.data.name == name)[0]
    }

    create(customerData: CustomerData): Customer {
        const id: string = uuid.v4()
        const customer = {id, data: customerData}
        this.customers.push(customer)
        return customer;
    }
    listAll(): Customer[] {
        return this.customers
    }

}

export {InMemoryCustomerRepository}