import express from 'express'
import { TemplateRenderer } from '../infrastructure/web/template/templateRenderer'
const htmlTemplateRenderer = TemplateRenderer.create()

const router = express.Router()


function randomIntFromInterval(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }


router.get('/slow', (req: any, res: any, next: any) => {
    setTimeout(() => { 
        const content = htmlTemplateRenderer.render("templates/slow.html", {})
        res.status(200).send(content)      
        next()
    }, randomIntFromInterval(500,2000))
})
router.get('/bad', (req: any, res: any, next: any) => {
    const randomInt = randomIntFromInterval(1,3)
    let message
    switch (randomInt){
        case 1:
            message = "conversion exception"
            break
        case 2: 
            message = "nullpointer exception"
            break
        case 3:
            message = "unexpected error"
            break
        default:
            console.log("This is not possible")
            break
    }
    throw new Error(message)
})



export { router as demoRoutes }