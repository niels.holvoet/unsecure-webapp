import { staticRoutes } from './static.routes'
import { demoRoutes } from './demo.routes'
import { userAccountSessionRepository, userAccountRepository } from "./../configuration/userAccount"
import {UserAccountSession} from "./../domain/userAccount"
import express from 'express'
import { TemplateRenderer } from '../infrastructure/web/template/templateRenderer'
const htmlTemplateRenderer = TemplateRenderer.create()


const router = express.Router()
router.use(staticRoutes)
router.use("/demo", demoRoutes)

router.get("/", (req: any, res: any, next: any) => {
    const content = htmlTemplateRenderer.render("templates/home.html", {})
    res.status(200).send(content)
})

router.post("/login", (req: any, res: any, next: any) => {
    const { username, password } = req.body
    const userAccountSession: UserAccountSession = userAccountSessionRepository.createSession(username, password)
    if (userAccountSession) {
        const { token, expiresAt } = userAccountSession
        res.cookie("session_token", token, { expires: expiresAt })
        res.redirect("/private")
    } else {
        const data = { validation: "Login Failed" }
        const content = htmlTemplateRenderer.render("templates/login.html", data)
        res.status(200).send(content)
        
    }
    next()
})



export { router as publicRoutes }