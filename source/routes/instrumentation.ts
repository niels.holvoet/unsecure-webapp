import express from 'express'
import Prometheus from 'prom-client'
import { TemplateRenderer } from '../infrastructure/web/template/templateRenderer'
const htmlTemplateRenderer = TemplateRenderer.create()
Prometheus.collectDefaultMetrics()

const httpRequestDurationMicroseconds = new Prometheus.Histogram({
    name: 'http_request_duration_ms',
    help: 'Duration of HTTP requests in ms',
    labelNames: ['method', 'route', 'code'],
    buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500,1000,3000]  // buckets for response time from 0.1ms to 500ms
})
const errorCounter = new Prometheus.Counter({
    name: 'errors',
    help: 'Number of errors',
    labelNames: ['method', 'route', 'message']
})
const requestCounter = new Prometheus.Counter({
    name: 'requests',
    help: 'Number of requests',
    labelNames: ['method', 'route']
})



const instrumentedRoutes = express.Router()


instrumentedRoutes.get('/metrics', async (req: any, res: any) => {
    res.set('Content-Type', Prometheus.register.contentType)
    const metrics = await Prometheus.register.metrics()
    res.end(metrics)
})

const instrumentError = (err: Error, req: any, res: any) => {
    const responseTimeInMs = Date.now() - res.locals.startEpoch
    httpRequestDurationMicroseconds
        .labels(req.method, req?.path, res.statusCode)
        .observe(responseTimeInMs)
    errorCounter.inc({ method: req.method, route: req?.path, message: err.message })

}

const errorHandler = (err: Error, req: any, res: any, next: any) => {
    instrumentError(err, req, res)
    console.error(err.stack)
    const content = htmlTemplateRenderer.render("templates/error.html")
    res.status(200).send(content)
    next()
}

instrumentedRoutes.use(errorHandler)



const instrumentRequest = ((req: any, res: any, next: any) => {
    res.locals.startEpoch = Date.now()
    requestCounter.inc({ method: req.method, route: req?.path })
    next()
})

const instrumentResponse = ((req: any, res: any, next: any) => {
    const responseTimeInMs = Date.now() - res.locals.startEpoch
    httpRequestDurationMicroseconds
        .labels(req.method, req?.path, res.statusCode)
        .observe(responseTimeInMs)
    next()
})



export { instrumentedRoutes, instrumentRequest, instrumentResponse, instrumentError }