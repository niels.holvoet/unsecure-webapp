import express from 'express'
import { TemplateRenderer } from '../infrastructure/web/template/templateRenderer'
import { userAccountSessionRepository, userAccountRepository } from "./../configuration/userAccount"
import {UserAccountSession} from "./../domain/userAccount"
const htmlTemplateRenderer = TemplateRenderer.create()

const authenticationHandler = (req: any, res: any, next: any) => {
    const cookies = req.cookies
    let isValidSession, sessionToken
    if (cookies) {
        sessionToken = cookies["session_token"]
        isValidSession = userAccountSessionRepository.isValidSession(sessionToken)
    } else {
        isValidSession = false
    }
    if (isValidSession) {
        const userAccountSession = userAccountSessionRepository.findByToken(sessionToken)
        req.userAccountSession = userAccountSession
        next()
    } else {
        const data = htmlTemplateRenderer.render("templates/login.html")
        res.status(200).send(data)
    }
    

}



const logoutHandler = (req: any, res: any, next: any) => {
    res.cookie("session_token", "", { expires: new Date() })
    res.redirect("/")
    next()
}

const userHandler = (req: any, res: any, next: any) => {
    const userAccountSession: UserAccountSession = req.userAccountSession
    const { username, role } = userAccountSession
    const data = { username: username, role: role }
    const content = htmlTemplateRenderer.render("templates/user.html", data)
    res.status(200).send(content)
    next()
}


const router = express.Router()
router.use("/", authenticationHandler)
router.get('/', userHandler)
router.post("/logout", logoutHandler)

export { router as privateRoutes }