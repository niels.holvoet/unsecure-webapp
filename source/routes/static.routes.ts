import express from 'express'
import path from 'path'
import {FileReader} from "./../infrastructure/file/fileReader"

const getStaticFolder = () : string => {
    const fileReader = FileReader.create()
    const resolvedPath = path.join(fileReader.getAppRootDir(), 'public')
    return resolvedPath
}

const staticRoutes = express.Router()

staticRoutes.use('/static', express.static(getStaticFolder()))

export {staticRoutes}

